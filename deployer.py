import os
import subprocess
import sys
from flask import Flask, request, abort, jsonify, Response
import subprocess
from datetime import date, datetime

pswd = sys.argv[1]

ip = sys.argv[2]

lport = sys.argv[3]

app = Flask(__name__)

@app.route( '/deploy', methods=['GET'])
def deploy():
    date = datetime.now()
    print(f"[{date}]\n", file=sys.stdout)
    service_name = request.args.get("name")
    os.chdir(service_name)
    subprocess.call(["./deploy.sh", pswd])
    os.chdir("..")
    print("\n\n", file=sys.stdout)
    return "Deploy OK!"

if __name__ == "__main__":
    app.run(host=ip, port=lport)



# Deployer
This service was made for the Hashiwokakero Project (INSA - 5SDBD - 2021/2022)
## Purpose

This service execute deploy script on HTTP request. 

## Installation
### Dependencie(s)
* Flask

### Other requirement(s)
* You have to create a deployment script named "deploy.sh" in each of the folder you want to deploy applications.


## Usage
Launch the deployer: ``` python deployer.py <root password> <ip> <port> ```

(The root password is needed in order to executate every commands from any deployment scripts)

You can request the deployment of an application "MyApp" situated in the folder ./MyApp by requesting the deployer: 

``` curl http://<ip>:<port>/deploy?name=MyApp  ```

